public class Parent {

    public String strParentPublic = "public";
    private String strParentPrivate = "private";

    public Integer intParentPublic = 0;
    private Integer intParentPrivate = 1;

}
