public class MyClass <T extends String> extends Parent{

    public T t1;
    public T t2;

    @MyAnnotation
    public String strPublic;

    public Long aLong;

    public double aDouble;

    public Float aFloat;

    @MyAnnotation
    private String strPrivate;

    public int intPublic = 3;

    @MyAnnotation
    private int intPrivate = 4;

    @MyAnnotation
    public T getT() {
        return t1;
    }

    @MyAnnotation
    public void setT(T t){
        this.t1 = t;
    }

    public  <V> V getg(V v){
        return v;
    }
}
