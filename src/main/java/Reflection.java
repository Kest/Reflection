import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.HashSet;

public class Reflection {

    public static Logger logger = Logger.getLogger(Reflection.class);

    public static HashSet<String> printFields(Class myClass, String type, boolean access, HashSet<String> result) {

        if (!myClass.getSuperclass().toString().contains("java.lang.Object")) {
            printFields(myClass.getSuperclass(), type, access, result);
        }

        Field[] fields;
        fields = myClass.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(access);
            if (fields[i].getType().toString().toLowerCase().contains(type.toLowerCase()))
                if (fields[i].getModifiers() == Modifier.PUBLIC || access) {
                    try {
                        result.add(fields[i].getName() + " = " + fields[i].get(myClass.newInstance()) + "");
                        System.out.println(fields[i].getName() + " = " + fields[i].get(myClass.newInstance()));
                    } catch (IllegalAccessException e) {
                        logger.error("Error: ", e);
                    } catch (InstantiationException e) {
                        logger.error("Error: ", e);
                    }

                }
        }
        return result;
    }

    public static HashSet<String> printAnnotatedItems(Class myClass, HashSet<String> result){
        Field[] fields = myClass.getDeclaredFields();
        Method[] methods = myClass.getMethods();

        for (int i = 0; i < fields.length; i++) {
            if (fields[i].isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation annotation = fields[i].getAnnotation(MyAnnotation.class);
                result.add(fields[i].getName() + " " + annotation.comment());
                System.out.println(fields[i].getName() + " " + annotation.comment());
            }
        }

        for (int i = 0; i < methods.length; i++) {
            if (methods[i].isAnnotationPresent(MyAnnotation.class)) {
                MyAnnotation annotation = methods[i].getAnnotation(MyAnnotation.class);
                result.add(methods[i].getName() + " " + annotation.comment() + "");
                System.out.println(methods[i].getName() + " " + annotation.comment());
            }
        }
        return result;
    }

    public static HashSet<String> printGeneric(Class myClass, HashSet<String> result){
        Field[] fields = myClass.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            if (!fields[i].getGenericType().toString().equals(fields[i].getType().toString())) {
                result.add(fields[i].getType() + ": " + fields[i].getName() + "");
                System.out.println(fields[i].getType() + ": " + fields[i].getName() + "");
            }
        }

        Type[] types;
        Method[] methods = myClass.getMethods();
        for (int i = 0; i < methods.length; i++) {
            if (methods[i].getGenericParameterTypes().toString().contains("reflect") && (!methods[i].getName().contains("getClass"))) {
                types = methods[i].getParameterTypes();
                if (types.length > 0) {
                    for (int j = 0; j < types.length; j++) {
                        result.add(methods[i].getName() + "(" + types[j].getTypeName() + ") -> " + methods[i].getReturnType() + "");
                        System.out.println(methods[i].getName() + "(" + types[j].getTypeName() + ") -> " + methods[i].getReturnType());
                    }
                } else {
                    result.add(methods[i].getName() + "() -> " + methods[i].getReturnType() + "");
                    System.out.println(methods[i].getName() + "() -> " + methods[i].getReturnType());
                }
            }
        }
        return result;
    }
}
