import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;

public class MyClassTest extends Assert {

    private static Logger logger = Logger.getLogger(MyClassTest.class);

    private static HashSet<String> printAllStringFields = new HashSet<>();
    private static HashSet<String> printAvailableIntegerFields = new HashSet<>();
    private static HashSet<String> printAnnotatedItems = new HashSet<>();
    private static HashSet<String> printGeneric = new HashSet<>();

    private HashSet<String> resultPrintAllStringFields = new HashSet<>();
    private HashSet<String> resultPrintAvailableIntegerFields = new HashSet<>();
    private HashSet<String> resultPrintAnnotatedItems = new HashSet<>();
    private HashSet<String> resultPrintGeneric = new HashSet<>();

    private static MyClass<String> myClass;


    @BeforeClass
    public static void writeSetsAndMyClass() {
        printAllStringFields.add("strParentPublic = public");
        printAllStringFields.add("strParentPrivate = private");
        printAllStringFields.add("t1 = null");
        printAllStringFields.add("t2 = null");
        printAllStringFields.add("strPublic = null");
        printAllStringFields.add("strPrivate = null");

        printAvailableIntegerFields.add("intParentPublic = 0");
        printAvailableIntegerFields.add("intPublic = 3");
        printAvailableIntegerFields.add("aLong = null");
        printAvailableIntegerFields.add("aFloat = null");
        printAvailableIntegerFields.add("aDouble = 0.0");

        printAnnotatedItems.add("strPublic MyAnnotation");
        printAnnotatedItems.add("strPrivate MyAnnotation");
        printAnnotatedItems.add("intPrivate MyAnnotation");
        printAnnotatedItems.add("getT MyAnnotation");
        printAnnotatedItems.add("setT MyAnnotation");

        printGeneric.add("class java.lang.String: t1");
        printGeneric.add("class java.lang.String: t2");
        printGeneric.add("getg(java.lang.Object) -> class java.lang.Object");
        printGeneric.add("getT() -> class java.lang.String");
        printGeneric.add("setT(java.lang.String) -> void");

        myClass = new MyClass<>();
    }

    @After
    public void afterTest() {
        System.out.println(">------------------------------------------------<");
    }

    @Test
    public void shouldPrintAvailableIntegerFields() {

        Reflection.printFields(myClass.getClass(), "int", false, resultPrintAvailableIntegerFields);
        Reflection.printFields(myClass.getClass(), "double", false, resultPrintAvailableIntegerFields);
        Reflection.printFields(myClass.getClass(), "float", false, resultPrintAvailableIntegerFields);
        Reflection.printFields(myClass.getClass(), "long", false, resultPrintAvailableIntegerFields);

        assertEquals(printAvailableIntegerFields, resultPrintAvailableIntegerFields);
    }

    @Test
    public void shouldPrintAllStringFields() {

        Reflection.printFields(myClass.getClass(), "String", true, resultPrintAllStringFields);

        assertEquals(printAllStringFields, resultPrintAllStringFields);
    }

    @Test
    public void shouldPrintAnnotatedItems() {

        Reflection.printAnnotatedItems(myClass.getClass(), resultPrintAnnotatedItems);

        assertEquals(printAnnotatedItems, resultPrintAnnotatedItems);
    }

    @Test
    public void shouldPrintGeneric() {
        Reflection.printGeneric(myClass.getClass(), resultPrintGeneric);
        assertEquals(printGeneric, resultPrintGeneric);
    }
}